package org.epam.shapes;;

public abstract class Shape {

    String shape;
    String color;

    public String getShape() {
        return shape;
    }

    public String getColor() {
        return color;
    }

    public Shape(String shape, String color) {
        this.shape = shape;
        this.color = color;
    }

    double square(double a) {
        return a;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "shape='" + shape + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}