package org.epam.shapes;


public  class Circle extends Shape{
    private static final double PI = 3.14;

    public Circle(String shape, String color) {
        super(shape, color);
    }

    public double square(double r){
        System.out.println("Square of circle: ");
        return PI * Math.sqrt(r);
    }
}
