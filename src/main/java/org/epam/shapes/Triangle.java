package org.epam.shapes;

public  class Triangle extends Shape {
    public Triangle(String shape, String color) {
        super(shape, color);
    }

    public double square(double a, double b,double c){
        System.out.println("Square of triangle: ");
        double p =(a+b+c)/2;
        return square(p*(p-a)*(p-b)*(p-c));
    }
}
