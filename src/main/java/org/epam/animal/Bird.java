package org.epam.animal;

public abstract class Bird implements Animal{

    Food food;
    String birdSpecies;
    String color;

    public Bird(String birdSpecies, String color, Food food) {
        this.birdSpecies = birdSpecies;
        this.color = color;
        this.food = food;
    }

    public String getBirdSpecies(){
        return birdSpecies;
    }

    void doHave(boolean feathers, boolean wings) {

    }

    abstract void fly();
}

