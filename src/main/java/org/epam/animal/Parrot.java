package org.epam.animal;

public class Parrot extends Bird {

    public Parrot(String birdSpecies, String color, Food food) {
        super(birdSpecies, color, food);
    }

    public void fly() {
        System.out.println("I'm " + birdSpecies+ " and I i believe i can fly!");
    }

    public void sleep() {
        System.out.println("Darknnes is comming");
    }

    public void eatAnything() {
        System.out.println("It't time have some " + food);
    }


    @Override
    public String toString() {
        return "Parrot{" +
                "food=" + food +
                ", birdSpecies='" + birdSpecies + '\'' +
                ", color='" + color + '\'';
    }
}
