package org.epam.animal;

public abstract class Fish implements Animal{
    String fishSpecies;
    boolean branchia;
    boolean squama;
    String color;

    abstract void swim();
}
