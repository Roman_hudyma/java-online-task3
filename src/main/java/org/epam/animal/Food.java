package org.epam.animal;

public enum Food {
    FRUITS, SEEDS, NUTS, INSECTS, BIRD, SNAKE, FISH
}