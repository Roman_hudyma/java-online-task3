package org.epam.animal;

public class Eagle extends Bird {

    public Eagle(String birdSpecies, String color,Food food) {
        super(birdSpecies, color, food);
    }

    public void fly() {
        System.out.println("I'm " + birdSpecies+ ", I'm flying");
    }

    public void sleep() {
        System.out.println("It's time to sleep");
    }

    public void eatAnything() {
        System.out.println("Yammi " + food);
    }


    @Override
    public String toString() {
        return "Eagle{" +
                "food=" + food +
                ", birdSpecies='" + birdSpecies + '\'' +
                ", color='" + color + '\'';
    }
}
